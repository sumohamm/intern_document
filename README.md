 
# Repositories Guid


## DAL_simgrid
it contains
- parse_simul_DAliuge: where all the code of parsing and simulate DAliuge
- PLatform: for simgrid execution
- Graph_json: conains some examples where the Json is generated for the simulation , Also micro benshmarks
- Tesh: for executing tests

## DAliuge_files
File provides all the data to use DAliuge
runDa.sh which is bash script that can simplify run and stop DAliuge


## Pubs
Contains the Bibliography , including all the PDFs

## Simgrid_base
contains :
- simgrid_tutorial: soluton of the suggested exercises in simgrid documentation
- master_forwarder_workers: simgrid to send messages from master to worker through forwarder and that is like data_drops
- DAX_master_forwarder_workers: simulate graph execution using simgrid wiht graph taken from DAX
