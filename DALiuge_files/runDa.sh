#!/bin/bash

if [ $1 == "run" ];
then 

cd daliuge-engine && ./run_engine.sh dev && cd ..
cd daliuge-translator && ./run_translator.sh dev >/dev/null && cd ..
#>/dev/null is used to hide crazy out put on the shell 


elif [ $1 == "stop" ];
then
# cd daliuge-translator && ./stop_translator.sh dev >/dev/null && cd ..
 cd daliuge-engine && ./stop_engine.sh dev && cd ..

#note docker ps will show the container runing 
# but to execute the  engin we need the id of it so it is parsed by parse engin line and take the first column where the id 

elif [ $1 == "ex" ];
then
#idc=$( docker ps | grep 'icrar/daliuge-engine'| cut -d ' ' -f1 )
#echo $id
#docker exec -it $idc  /bin/sh | exit

user=$(whoami)
cd /home/$user/dlg/workspace && cd $(ls -t | head -1) # ls -t sort by date , then head 1 chose the most recent and  $() naviagte  
  if [ $2 == "def" ];
  then
    cp -R . /home/$user/Desktop
  elif [ $2  != "def"];
  then 
    cp -R . $2
  fi

fi


# remeber variable in bash script like: myvar=$( ls /etc | wc -l )
#remeber after any change to save and ensure 
    #chmod u+x runDa.sh 
