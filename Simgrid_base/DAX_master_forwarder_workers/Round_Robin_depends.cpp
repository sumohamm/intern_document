#include <math.h>
#include <simgrid/host.h>
#include <simgrid/s4u.hpp>
#include <string.h>

XBT_LOG_NEW_DEFAULT_CATEGORY(dag_scheduling, "Logging specific to this example");
namespace sg4 = simgrid::s4u;

/* 
remember: dag represent the workflow of the tasks
          platform represent the hosts  
get_ready_tasks :Browse the dag and push in the array all the tasks that are  A computation 
schedule_on : execute the task 
*/

struct HostAttribute {
  /* Earliest time at which a host is ready to execute a task */
  double available_at                     = 0.0;
  sg4::Exec* last_scheduled_task          = nullptr;
};


static double sg_host_get_available_at(const sg4::Host* host){
  return host->get_data<HostAttribute>()->available_at;
}

static void sg_host_set_available_at(const sg4::Host* host, double time){
  host->get_data<HostAttribute>()->available_at = time;
}

static sg4::Exec* sg_host_get_last_scheduled_task(const sg4::Host* host){
  return host->get_data<HostAttribute>()->last_scheduled_task;
}

static void sg_host_set_last_scheduled_task(const sg4::Host* host, sg4::ExecPtr task){
  host->get_data<HostAttribute>()->last_scheduled_task = task.get();
}


static bool dependency_exists(const sg4::Exec* src, sg4::Exec* dst)
{
  const auto& dependencies = src->get_dependencies();
  const auto& successors   = src->get_successors();
  return (std::find(successors.begin(), successors.end(), dst) != successors.end() ||
          dependencies.find(dst) != dependencies.end());
}

static std::vector<sg4::Exec*> get_ready_tasks(const std::vector<sg4::ActivityPtr>& dax){
  std::vector<sg4::Exec*> ready_tasks;
  std::map<sg4::Exec*, unsigned int> candidate_execs;

  for (auto& a : dax) {
    // Only look at activity that have their dependencies solved but are not assigned
    if (a->dependencies_solved() && not a->is_assigned()) {
      // if it is an exec, it's ready
      auto* exec = dynamic_cast<sg4::Exec*>(a.get());
      if (exec != nullptr)
        ready_tasks.push_back(exec);
      // if it a comm, we consider its successor as a candidate. If a candidate solves all its dependencies,
      // i.e., get all its input data, it's ready
      const auto* comm = dynamic_cast<sg4::Comm*>(a.get());
      if (comm != nullptr) {
        auto* next_exec = static_cast<sg4::Exec*>(comm->get_successors().front().get());
        candidate_execs[next_exec]++;
        if (next_exec->get_dependencies().size() == candidate_execs[next_exec])
          ready_tasks.push_back(next_exec);
      }
    }
  }
  XBT_DEBUG("There are %zu ready tasks", ready_tasks.size());
  return ready_tasks;
}


static void schedule_on(sg4::ExecPtr exec, sg4::Host* host){
  exec->set_host(host);
  // we can also set the destination of all the input comms of this exec
  for (const auto& pred : exec->get_dependencies()) {
    auto* comm = dynamic_cast<sg4::Comm*>(pred.get());
    if (comm != nullptr) {
      comm->set_destination(host);
      delete comm->get_data<double>();
    }
  }
  // we can also set the source of all the output comms of this exec
  for (const auto& succ : exec->get_successors()) {
    auto* comm = dynamic_cast<sg4::Comm*>(succ.get());
    if (comm != nullptr)
      comm->set_source(host);
  }
}


int main(int argc, char **argv){

sg4::Engine e(&argc, argv);  // start the model
std::set<sg4::Activity*> vetoed;
e.track_vetoed_activities(&vetoed);

e.load_platform(argv[1]);// read the platform 

unsigned long total_nhosts = e.get_host_count();  // number of hosts
const auto hosts          = e.get_all_hosts(); // names of hosts

std::vector<HostAttribute> host_attributes(total_nhosts);
for (unsigned long i = 0; i < total_nhosts; i++)
  hosts[i]->set_data(&host_attributes[i]);
  
auto dax = sg4::create_DAG_from_DAX(argv[2]);// read the file of dag/* load the DAX file  as xbt_dynar_t variable */

/* Schedule the root first */
auto* root = static_cast<sg4::Exec*>(dax.front().get());
auto hostt  = hosts[0];
schedule_on(root, hostt);

XBT_INFO("Schedule %s on %s", root->get_cname(), hostt->get_cname());
e.run();


while (not vetoed.empty()) {
    XBT_DEBUG("Start new scheduling round");
    /* Get the set of ready tasks */
    auto ready_tasks = get_ready_tasks(dax);
    vetoed.clear();

    if (ready_tasks.empty()) {
      /* there is no ready task, let advance the simulation */
      e.run();
      continue;
    }

    sg4::Exec* selected_task          = nullptr;
    sg4::Host* selected_host          = nullptr;

      
  int cp=0;
  for (auto task : ready_tasks) {

    XBT_DEBUG("%s is ready", task->get_cname());
    auto host               = hosts[cp]; //available_host(hosts, total_nhosts);
    selected_task   = task;
    selected_host   = host;
    cp++;
  }

  XBT_INFO("Schedule %s on %s", selected_task->get_cname(), selected_host->get_cname());
  
  schedule_on(selected_task, selected_host);

  /*auto last_scheduled_task = sg_host_get_last_scheduled_task(selected_host);
  if (last_scheduled_task && (last_scheduled_task->get_state() != sg4::Activity::State::FINISHED) &&
        (last_scheduled_task->get_state() != sg4::Activity::State::FAILED) &&
        not dependency_exists(sg_host_get_last_scheduled_task(selected_host), selected_task))
    last_scheduled_task->add_successor(selected_task);
  */ 
   
  sg_host_set_last_scheduled_task(selected_host, selected_task);

  ready_tasks.clear();
  e.run();
}

XBT_INFO("Simulation Time: %f", simgrid_get_clock());

return 0;
}
