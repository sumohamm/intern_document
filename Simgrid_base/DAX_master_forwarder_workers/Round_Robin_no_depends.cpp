#include <math.h>
#include <simgrid/host.h>
#include <simgrid/s4u.hpp>
#include <string.h>

XBT_LOG_NEW_DEFAULT_CATEGORY(dag_scheduling, "Logging specific to this example");
namespace sg4 = simgrid::s4u;

/* 
remember: dag represent the workflow of the tasks
          platform represent the hosts  
get_ready_tasks :Browse the dag and push in the array all the tasks that are ready 
schedule_on : execute the task 
*/

static std::vector<sg4::Exec*> get_ready_tasks(const std::vector<sg4::ActivityPtr>& dax){
  std::vector<sg4::Exec*> ready_tasks;
  std::map<sg4::Exec*, unsigned int> candidate_execs;

  for (auto& a : dax) {
    // Only look at activity that have their dependencies solved but are not assigned
    if ( not a->is_assigned()) {
      // if it is an exec, it's ready
      auto* exec = dynamic_cast<sg4::Exec*>(a.get());
      if (exec != nullptr)
        ready_tasks.push_back(exec);
    }
  }
  XBT_DEBUG("There are %zu ready tasks", ready_tasks.size());
  return ready_tasks;
}


static void schedule_on(sg4::ExecPtr exec, sg4::Host* host){
  exec->set_host(host);
}


int main(int argc, char **argv){

sg4::Engine e(&argc, argv);  // start the model
std::set<sg4::Activity*> vetoed;
e.track_vetoed_activities(&vetoed);

e.load_platform(argv[1]);// read the platform 

//unsigned long total_nhosts = e.get_host_count();  // number of hosts
const auto hosts          = e.get_all_hosts(); // names of hosts
  
auto dax = sg4::create_DAG_from_DAX(argv[2]);// read the file of dag/* load the DAX file  as xbt_dynar_t variable */


/* Schedule the root first */
auto* root = static_cast<sg4::Exec*>(dax.front().get());
auto host_root  = hosts[0];
schedule_on(root, host_root);
XBT_INFO("Schedule %s on %s", root->get_cname(), host_root->get_cname());
e.run();

while (not vetoed.empty()) {
    XBT_DEBUG("Start new scheduling round");
    /* Get the set of ready tasks */
    auto ready_tasks = get_ready_tasks(dax);
    vetoed.clear();

    if (ready_tasks.empty()) {
      /* there is no ready task, let advance the simulation */
      e.run();
      continue;
    }

    sg4::Exec* selected_task          = nullptr;
    sg4::Host* selected_host          = nullptr;
    unsigned long cp=0;
  
   for (auto task : ready_tasks) {

   // XBT_DEBUG("%s is ready", task->get_cname());
    auto host               = hosts[cp]; //available_host(hosts, total_nhosts);
    selected_task   = task;
    selected_host   = host;
    cp++;
   }

   XBT_INFO("Schedule %s on %s", selected_task->get_cname(), selected_host->get_cname());
  
   schedule_on(selected_task, selected_host);

   ready_tasks.clear();
   e.run();
 }

XBT_INFO("Simulation Time: %f", simgrid_get_clock());

return 0;
}
