.. _usecase_smpi:

Simulating DAG Scheduling Algorithms
====================================
.. image:: ./fig/1.svg
   :align: center

Directed acyclic graphs scheduling  is about giving  resources to perform tasks basing on diagram
defines the execution order in meaning of respecting the precedence constraints so a task cannot
start before all its predecessors have completed.

There are two types of dependencies:

- Control flow dependency: where a task needs the predecessors to complete so it will be able to be executed .
- Data flow dependency: there is data need to be transferred from task “a” to task “b”.



Describe Workflows using Dax file
----------------------------------
DAX format,  Format of workflow used by Pegasus (http://pegasus.isi.edu/)
It is Xml file characterize the tasks and the predecessors of each one.
it consists of three sections:

Header: contains Name space and schema declaration (from Pegasus),Name of the DAX ,
Number of jobs: jobCount and Number of control dependencies: childCount

Job description: described by: id, name, runtime, input and output files

Control flow dependencies:
It represents the relation between tasks, by indexing each task is a parent for what task

For example: consider this workflow in the figure.

- C1 runs for 10 seconds, that requires an input file of 2e8 bytes and produces an output file of 5e8 bytes.
- C2 runs for 20 seconds, that requires an input file of 1e8 bytes, no output file.
- C3 runs for 20 seconds, and requires an input file of 5e8 bytes and produces an output file of 2e8 bytes.

.. image:: ./fig/2.svg
   :width: 200
   :align: right


.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <adag xmlns="http://pegasus.isi.edu/schema/DAX"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://pegasus.isi.edu/schema/DAX
    http://pegasus.isi.edu/schema/dax-2.1.xsd"
    version="2.1" count="1" index="0" name="smalldax"

    jobCount="3" fileCount="0" childCount="1">
    <job id="1" namespace="SG" name="c1" version="1.0" runtime="10">
    <uses file="i1" link="input" register="true" transfer="true"
    optional="false" type="data" size="2e8"/>
    <uses file="o1" link="output" register="true" transfer="true" optional="false" type="data" size="5e8"/>
    </job>
    <job id="2" namespace="SG" name="c2" version="1.0" runtime="20">
    <uses file="i2" link="input" register="true" transfer="true"optional="false" type="data" size="1e8"/>
    </job>
    <job id="3" namespace="SG" name="c3" version="1.0" runtime="20">
    <uses file="o1" link="input" register="true" transfer="true" optional="false" type="data" size="5e8"/>
    <uses file="o3" link="output" register="true" transfer="true" optional="false" type="data" size="2e8"/>
    </job>

    <child ref="3">
        <parent ref="2"/>
    </child>
    </adag>

For more information see `Pegasus <https://pegasus.isi.edu/documentation/development/schemas.html?highlight=xml>`_

Simulate DAG Scheduling on SimGrid
-----------------------------------
Two examples will be shown, first on Simulating the scheduling task without dependencies which means free tasks,
and second example will show readl DAG application like the one of Dax file discussed above

Tasks without dependencies
...........................





Tasks without dependencies
...........................




To schedule these tasks we still need to provide SimGrid with the platform we want these tasks to be executed on.
This can be done using platform file written in xml, like the one listed below.

.. code-block:: xml

    <?xml version='1.0'?>
    <!DOCTYPE platform SYSTEM "https://simgrid.org/simgrid.dtd">
    <platform version="4.1">
    <zone  id="AS0" routing="Full">
    <host id="Host 26" speed="3.300140519709234Gf"/>
    <host id="Host 27" speed="3.867398877553016Gf"/>
    <host id="Host 28" speed="1.6522665718098645Gf"/>
    <host id="Host 29" speed="1.0759376792481766Gf"/>
    <host id="Host 30" speed="2.4818410475340424Gf"/>
    <host id="Host 31" speed="1.773869555571436Gf"/>
    <host id="Host 32" speed="1.7843609176927505Gf"/>

    <link id="l152" bandwidth="125MBps" latency="100us"/>
    <link id="l153" bandwidth="125MBps" latency="100us"/>
    <link id="l154" bandwidth="125MBps" latency="100us"/>
    <link id="l155" bandwidth="125MBps" latency="100us"/>
    <link id="l156" bandwidth="125MBps" latency="100us"/>
    <link id="l157" bandwidth="125MBps" latency="100us"/>
    <link id="l159" bandwidth="125MBps" latency="100us"/>
    <link id="l160" bandwidth="125MBps" latency="100us"/>
    <link id="l161" bandwidth="125MBps" latency="100us"/>
    <link id="l162" bandwidth="125MBps" latency="100us"/>
    <link id="l163" bandwidth="125MBps" latency="100us"/>
    <link id="l164" bandwidth="125MBps" latency="100us"/>
    <link id="l165" bandwidth="125MBps" latency="100us"/>
    <link id="l166" bandwidth="125MBps" latency="100us"/>
    <link id="l167" bandwidth="125MBps" latency="100us"/>
    <link id="l168" bandwidth="125MBps" latency="100us"/>
    <link id="l169" bandwidth="125MBps" latency="100us"/>
    <link id="l170" bandwidth="125MBps" latency="100us"/>
    <link id="l171" bandwidth="125MBps" latency="100us"/>
    <link id="l172" bandwidth="125MBps" latency="100us"/>
    <link id="l173" bandwidth="125MBps" latency="100us"/>

    <route src="Host 26" dst="Host 27">
      <link_ctn id="l155"/>
    </route>
    <route src="Host 26" dst="Host 28">
      <link_ctn id="l155"/>
      <link_ctn id="l154"/>
      <link_ctn id="l156"/>
    </route>
    <route src="Host 26" dst="Host 29">
      <link_ctn id="l152"/>
      <link_ctn id="l157"/>
    </route>
    <route src="Host 26" dst="Host 30">
      <link_ctn id="l152"/>
      <link_ctn id="l161"/>
    </route>
    <route src="Host 26" dst="Host 31">
      <link_ctn id="l166"/>
    </route>
    <route src="Host 26" dst="Host 32">
      <link_ctn id="l152"/>
      <link_ctn id="l169"/>
    </route>
    <route src="Host 27" dst="Host 28">
      <link_ctn id="l154"/>
      <link_ctn id="l156"/>
    </route>
    <route src="Host 27" dst="Host 29">
      <link_ctn id="l159"/>
    </route>
    <route src="Host 27" dst="Host 30">
      <link_ctn id="l162"/>
    </route>
    <route src="Host 27" dst="Host 31">
      <link_ctn id="l167"/>
    </route>
    <route src="Host 27" dst="Host 32">
      <link_ctn id="l154"/>
      <link_ctn id="l170"/>
    </route>
    <route src="Host 28" dst="Host 29">
      <link_ctn id="l160"/>
    </route>
    <route src="Host 28" dst="Host 30">
      <link_ctn id="l163"/>
    </route>
    <route src="Host 28" dst="Host 31">
      <link_ctn id="l163"/>
      <link_ctn id="l168"/>
    </route>
    <route src="Host 28" dst="Host 32">
      <link_ctn id="l156"/>
      <link_ctn id="l170"/>
    </route>
    <route src="Host 29" dst="Host 30">
      <link_ctn id="l164"/>
    </route>
    <route src="Host 29" dst="Host 31">
      <link_ctn id="l159"/>
      <link_ctn id="l167"/>
    </route>
    <route src="Host 29" dst="Host 32">
      <link_ctn id="l171"/>
    </route>
    <route src="Host 30" dst="Host 31">
      <link_ctn id="l168"/>
    </route>
    <route src="Host 30" dst="Host 32">
      <link_ctn id="l172"/>
    </route>
    <route src="Host 31" dst="Host 32">
      <link_ctn id="l173"/>
    </route>
    </zone>
    </platform>





..  LocalWords:  SimGrid



