#include <iostream>
#include "simgrid/s4u.hpp"
#include "simgrid/kernel/ProfileBuilder.hpp"

#include <cstdlib>
#include <vector>
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");
long nb_hosts;
std::vector<simgrid::s4u::Host*> hosts; //needd to be global to have it in the master 
                                        //because the host name need the engin  
std::vector<simgrid::s4u::Link*> links;


static void worker()
{  

  simgrid::s4u::Mailbox* mailbox   = simgrid::s4u::Mailbox::by_name("center"); // notice here set the name of the mail box associated to arg[1] in each actor of the host
  //XBT_INFO("Worker ID %d", simgrid::s4u::this_actor::get_pid() );

  double compute_cost;
  double* msg; 

  do {
  try{
    //auto msg     = mailbox->get_unique<double>(); 
   simgrid::s4u::CommPtr comm = mailbox->get_async<double>(&msg);
    comm->wait();
    compute_cost = *msg;
    

    if (compute_cost > 0) /* If compute_cost is valid, execute a computation of that cost */
      simgrid::s4u::this_actor::execute(compute_cost);
   } catch (const simgrid::NetworkFailureException&) {
      XBT_INFO("Mmh. Something went wrong. Nevermind. Let's keep going!");
    }

  } while (compute_cost > 0); /* Stop when receiving an invalid compute_cost */
  
  delete msg; 
  XBT_INFO("Exiting now.");
}




static void master(std::vector<std::string> args)
{
  //xbt_assert(args.size() > 4, "The master function expects at least 3 arguments");

  long Tasks_count          = std::stol(args[1]);
  double compute_cost       = std::stod(args[2]);
  long communication_cost   = std::stol(args[3]);

  // notice here i= 4 to args.size() and then by_name(args[i])), because the firt three args are count ,comp_count,commcost,
  // and then after arg[4] until the end are the name of hosts of forowrders , see deploy file

  std::vector<simgrid::s4u::ActorPtr> actor;
  for(std::size_t i = 0; i < hosts.size(); ++i) {

   actor.push_back (simgrid::s4u::Actor::create("worker--"+std::to_string(i)
                   ,simgrid::s4u::Host::by_name(hosts[i]->get_cname()), worker)) ;  // simgrid::s4u::Host::by_name = simgrid::s4u::Engine::host_by_name
  }

  simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name("center");

  for (int i = 0; i < Tasks_count; i++) { /* For each task to be executed: */
    /* - Send the computation cost to that forwarders */
    if(i==Tasks_count/4) simgrid::s4u::Link::by_name("1")->turn_off();
    if(i==(Tasks_count/2)) simgrid::s4u::Link::by_name("1")->turn_on();

   try {
    //XBT_INFO("Sending to worker %d of %ld to mailbox '%s'", i, Tasks_count, mailbox->get_cname());
    simgrid::s4u::CommPtr comm = mailbox->put_async(new double(compute_cost) , communication_cost);
    //while (not comm->test()) ;
    comm->wait();
   } catch (const simgrid::NetworkFailureException&){
      XBT_INFO("Mmh. The communication with '%s' failed. Nevermind. Let's keep going!", mailbox->get_cname());
   }

  }
  
  for (int i = 0; i < nb_hosts; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */
    simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name("center" );
    try{
    //XBT_INFO("Sending to done %d of %ld to mailbox '%s'", i, Tasks_count, mailbox->get_cname());
    simgrid::s4u::CommPtr comm = mailbox->put_async(new double(-1) , 0.0);
    //while (not comm->test()) ;
    comm->wait();
    } catch (const simgrid::NetworkFailureException&){
      XBT_INFO("Mmh. The communication with '%s' failed. Nevermind. Let's keep going!", mailbox->get_cname());
   }
  } 
  
}


int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);

  /* Register the functions representing the actors */
  e.register_function("master", &master);
  /* Load the platform description and then deploy the application */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
  // be careful if you add a host here you need to have link so they can send message 
  // //  e.get_netzone_root()
  //     ->create_host("Lilibeth", 25e6)
  //     ->set_speed_profile(simgrid::kernel::profile::ProfileBuilder::from_string("lilibeth_profile", R"(
  //     0 1.0
  //     2 0.5
  //           )",2))->seal();
  
  hosts=e.get_all_hosts(); //global 
  links= e.get_all_links();
 /*
 Each event of the profile represent a peak speed change that is due to external load.
 The values are given as a rate of the initial value. 
 This means that the actual value is obtained by multiplying the 
 initial value (the peek speed at this pstate level) 
 by the rate coming from the profile.
 */
 // XBT_INFO( hosts[3]->get_cname());// Ginette
  simgrid::s4u::Host::by_name("Ginette")->set_speed_profile(simgrid::kernel::profile::ProfileBuilder::from_string("lilibeth_profile", 
  R"(0 0.8
     1 0.1
     1 0.5    )",40));//->seal() seal to prevent any more configuraion 

  nb_hosts= e.get_host_count(); // global var number of hosts which will be number of actors 

  std::vector<simgrid::s4u::Link*>rout_links= e.get_all_links();

  //simgrid::s4u::Host::route_to(simgrid::s4u::Host::by_name("Ginette"),rout_links, latency );   
  
  simgrid::s4u::Link::by_name("145")->set_bandwidth(10000000);
  //XBT_INFO("latency %f",simgrid::s4u::Link::by_name("4")->get_latency());
  simgrid::s4u::Link::by_name("4")->set_latency(1);
  
  // simgrid::s4u::Link::by_name("2")->set_bandwidth_profile(simgrid::kernel::profile::ProfileBuilder::from_string("4_profile", 
  // R"(0 0.0
  //    1 0.5    )",10)); //->seal() ; // better to use it for new created link because the link in the platform are sealed 

  // Dealing with failures
  
  /*simgrid::s4u::Link::by_name("4")->set_state_profile(simgrid::kernel::profile::ProfileBuilder::from_string("link4_profile", R"(
   4 0
   5 1
      )",10));
  */

  /* Run the simulation */ 
  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}
