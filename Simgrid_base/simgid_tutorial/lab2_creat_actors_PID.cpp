#include <iostream>
#include "simgrid/s4u.hpp"
#include <cstdlib>
#include <vector>
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");

long nb_hosts;
std::vector<simgrid::s4u::Host*> hosts; //needd to be global to have it in the master 
                                        //because the host name need the engin  

static void worker()
{  

  simgrid::s4u::Mailbox* mailbox   = simgrid::s4u::Mailbox::by_name(std::to_string( simgrid::s4u::this_actor::get_pid()) ); // notice here set the name of the mail box associated to arg[1] in each actor of the host
  
  XBT_INFO("Worker ID %d", simgrid::s4u::this_actor::get_pid() );

  double compute_cost;
  do {
    auto msg     = mailbox->get_unique<double>();
    compute_cost = *msg;
    
    if (compute_cost > 0) /* If compute_cost is valid, execute a computation of that cost */
      simgrid::s4u::this_actor::execute(compute_cost);
  } while (compute_cost > 0); /* Stop when receiving an invalid compute_cost */
  
  XBT_INFO("Exiting now.");

}



static void master(std::vector<std::string> args)
{  
  long Tasks_count          = std::stol(args[1]);
  double compute_cost       = std::stod(args[2]);
  long communication_cost   = std::stol(args[3]);

  XBT_INFO("Master ID:  %d", simgrid::s4u::this_actor::get_pid() );
    
  std::vector<simgrid::s4u::ActorPtr> actor;
  for(std::size_t i = 0; i < hosts.size(); ++i) {

   actor.push_back (simgrid::s4u::Actor::create("worker--"+std::to_string(i)
                   ,simgrid::s4u::Host::by_name(hosts[i]->get_cname()), worker)) ;  // simgrid::s4u::Host::by_name = simgrid::s4u::Engine::host_by_name
  }

  for (int i = 0; i < Tasks_count; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */
     simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(std::to_string(i % nb_hosts +2 ));
      /* - Send the computation cost to that forwarders */
     
     XBT_INFO("Sending task %d of %ld to mailbox '%s'", i, Tasks_count, mailbox->get_cname());
    
     //XBT_INFO("master actor  %d", simgrid::s4u::this_actor::get_pid() );

     mailbox->put(new double(compute_cost) , communication_cost);
  }
  
  //note it is important to notice that actors number starts from 1 not zero 
  //i starts from 2 because first actor is the master 
  // notice that you don not need t care anymore about the host name 
  for (int i = 2; i < nb_hosts+2; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */
    simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(std::to_string(i) );
    //XBT_INFO("Sending to done %d  to mailbox '%s'", i, mailbox->get_cname());
    mailbox->put(new double(-1.0),0.0);
  } 
  
}



int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  //xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);
  
  /* Register the functions representing the actors */
  e.register_function("master", &master);
  

  /* Load the platform description and then deploy the application */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
  
  

  hosts=e.get_all_hosts(); //global 
  nb_hosts= e.get_host_count(); // global var number of hosts which will be number of actors 
  
  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}
