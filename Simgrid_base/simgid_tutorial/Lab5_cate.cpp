#include <iostream>
#include "simgrid/s4u.hpp"
#include <cstdlib>
#include <vector>
#include <string>
#include "simgrid/instr.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");


long nb_hosts;
std::vector<simgrid::s4u::Host*> hosts; //needd to be global to have it in the master 
                                        //because the host name need the engin  
double timout; 
std::vector<simgrid::s4u::ActorPtr> actor;

static void worker()
{  
  long number_task =0;
  double start= simgrid::s4u::Engine::get_clock();
  simgrid::s4u::Mailbox* mailbox   = simgrid::s4u::Mailbox::by_name
                                      (std::to_string( simgrid::s4u::this_actor::get_pid()) ); // notice here set the name of the mail box associated to arg[1] in each actor of the host
  
  XBT_INFO("%s ID %d", simgrid::s4u::this_actor::get_cname(),  simgrid::s4u::this_actor::get_pid()  );

  double compute_cost;
    auto msg     = mailbox->get_unique<double>();
    compute_cost = *msg;
  // notice do here is just for execution because the message sent only once to each actor  
  do {
    simgrid::s4u::this_actor::execute(compute_cost);
    
    // simgrid::s4u::ExecPtr exec = simgrid::s4u::this_actor::exec_init(compute_cost);
    // exec->set_tracing_category("comput");
    // // exec->start() is optional here as wait() starts the activity on need
    // exec->wait();
 
    //XBT_INFO("Worker ID %d time is %f", simgrid::s4u::this_actor::get_pid(),simgrid::s4u::Engine::get_clock() - start  );
    number_task++;
  } while (simgrid::s4u::Engine::get_clock() - start < timout); /* Stop when receiving an invalid compute_cost */

  XBT_INFO("Exit Worker ID %d with number_task %d", simgrid::s4u::this_actor::get_pid(), number_task);
}



static void master(std::vector<std::string> args)
{
  //xbt_assert(args.size() > 4, "The master function expects at least 3 arguments");
  
  timout                    = std::stol(args[1]);
  double compute_cost       = std::stod(args[2]);
  long communication_cost   = std::stol(args[3]);
  
  XBT_INFO("%s ID %d", simgrid::s4u::this_actor::get_cname(),  simgrid::s4u::this_actor::get_pid()  );


  for (int i = 4 ; i <nb_hosts+4 ; i++) { /* For each task to be executed: */

     simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(std::to_string(i)); // first actoe is the master so need to start from 2
      /* - Send the computation cost to that forwarders */
     
     XBT_INFO("Sending from master %s  to mailbox %s", simgrid::s4u::this_actor::get_cname() , mailbox->get_cname());
    
     mailbox->put(new double(compute_cost) , communication_cost);
   }
 
}


int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  //xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);
  
  simgrid::instr::declare_tracing_category("compute", "1 0 0");  // red
  simgrid::instr::declare_tracing_category("request", "0 1 0");  // green
  simgrid::instr::declare_tracing_category("data", "0 0 1");     // blue
  simgrid::instr::declare_tracing_category("finalize", "0 0 0"); // black

  /* Register the functions representing the actors */
  
  e.register_function("master", &master);


  /* Load the platform description and then deploy the application */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
  


  hosts=e.get_all_hosts(); //global 
  nb_hosts= e.get_host_count(); // global var number of hosts which will be number of actors 
  
    for(std::size_t i = 0; i < hosts.size(); ++i) {

   actor.push_back (simgrid::s4u::Actor::create("W-"+ hosts[i]->get_name()
                   ,simgrid::s4u::Host::by_name(hosts[i]->get_cname()), worker)) ;  // simgrid::s4u::Host::by_name = simgrid::s4u::Engine::host_by_name
  }

  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}

  
