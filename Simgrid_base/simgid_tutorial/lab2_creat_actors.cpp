#include <iostream>
#include "simgrid/s4u.hpp"
#include <cstdlib>
#include <vector>
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");

long nb_hosts;

//since you are creating the worker from the coe you can use any more std::vector<std::string> args
// because before the argument taken from the input file , but creating everything here you can not use it anymore 

static void worker(std::string id)
{  

  simgrid::s4u::Mailbox* mailbox   = simgrid::s4u::Mailbox::by_name("worker-"+ id); // notice here set the name of the mail box associated to arg[1] in each actor of the host

  double compute_cost;
  do {
    auto msg     = mailbox->get_unique<double>();
    compute_cost = *msg;
    
    if (compute_cost > 0) /* If compute_cost is valid, execute a computation of that cost */
      simgrid::s4u::this_actor::execute(compute_cost);
  } while (compute_cost > 0); /* Stop when receiving an invalid compute_cost */

  XBT_INFO("Exiting now.");
}



static void master(std::vector<std::string> args)
{
  //xbt_assert(args.size() > 4, "The master function expects at least 3 arguments");
  
  long Tasks_count          = std::stol(args[1]);
  double compute_cost       = std::stod(args[2]);
  long communication_cost   = std::stol(args[3]);
  
  //long host_num=e.get_host_count();

  // notice here i= 4 to args.size() and then by_name(args[i])), because the firt three args are count ,comp_count,commcost,
  // and then after arg[4] until the end are the name of hosts of forowrders , see deploy file

  //XBT_INFO("nb_host", get_host_count() );
  //auto actors = simgrid::s4u::Actor::create("worker", simgrid::s4u::Host::by_name("Fafard"), worker,"1");
  
  for (int i = 0; i < Tasks_count; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */  
    std::string worker_rank          = std::to_string(i% nb_hosts);
    simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name("worker-"+worker_rank );

    /* - Send the computation cost to that forwarders */
    XBT_INFO("Sending to worker %d of %ld to mailbox '%s'", i, Tasks_count, mailbox->get_cname());
    mailbox->put(new double(compute_cost) , communication_cost);
  }
  
  for (int i = 0; i < nb_hosts; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */
    simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name("worker-"+  std::to_string(i) );
    //XBT_INFO("Sending to done %d of %ld to mailbox '%s'", i, Tasks_count, mailbox->get_cname());
    mailbox->put(new double(-1.0),0.0);
  } 
  
}


int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  //xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);
  
  /* Register the functions representing the actors */
  e.register_function("master", &master);

  /* Load the platform description and then deploy the application */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
  
  nb_hosts= e.get_host_count(); // global varnumber of hosts which will be number of actors 
  
  //XBT_INFO("nb_host= %d", nb_hosts );

  std::vector<simgrid::s4u::Host*> hosts =e.get_all_hosts();
  std::vector<simgrid::s4u::ActorPtr> actor;

 for(std::size_t i = 0; i < hosts.size(); ++i) {
   //XBT_INFO("hosts%s",hosts[i]->get_cname()) ;
   actor.push_back (simgrid::s4u::Actor::create("worker-"+std::to_string(i)
     , simgrid::s4u::Host::by_name(hosts[i]->get_cname()), worker, std::to_string(i)) ) ;  // simgrid::s4u::Host::by_name = simgrid::s4u::Engine::host_by_name
 }
  
  //XBT_INFO("nb_actors= %d",  e.get_host_count() );

  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}
