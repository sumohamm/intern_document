#include <iostream>
#include "simgrid/s4u.hpp"
#include <cstdlib>
#include <vector>
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");

static void master(std::vector<std::string> args)
{
  //xbt_assert(args.size() > 4, "The master function expects at least 3 arguments");

  long workers_count        = std::stol(args[1]); // number of worker
  long Tasks_count          = std::stol(args[2]);
  double compute_cost       = std::stod(args[3]);
  long communication_cost   = std::stol(args[4]);

  // notice here i= 4 to args.size() and then by_name(args[i])), because the firt three args are count ,comp_count,commcost,
  // and then after arg[4] until the end are the name of hosts of forowrders , see deploy file

  //XBT_INFO("Got %zu forwarders and %ld tasks to process", forwarders.size(), tasks_count);

  for (int i = 0; i < Tasks_count; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */  
    std::string worker_rank          = std::to_string(i % workers_count);
    simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name("worker-"+worker_rank );

    /* - Send the computation cost to that forwarders */
    XBT_INFO("Sending to worker %d of %ld to mailbox '%s'", i, Tasks_count, mailbox->get_cname());
    mailbox->put(new double(compute_cost) , communication_cost);
  }
  
  for (int i = 0; i < workers_count; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */
    simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name("worker-"+  std::to_string(i) );
    //XBT_INFO("Sending to done %d of %ld to mailbox '%s'", i, Tasks_count, mailbox->get_cname());
    mailbox->put(new double(-1.0),0.0);
  } 
  
}



static void worker(std::vector<std::string> args)
{  

  simgrid::s4u::Mailbox* mailbox   = simgrid::s4u::Mailbox::by_name("worker-"+ args[1]); // notice here set the name of the mail box associated to arg[1] in each actor of the host

  double compute_cost;
  do {
    auto msg     = mailbox->get_unique<double>();
    compute_cost = *msg;
    
    if (compute_cost > 0) /* If compute_cost is valid, execute a computation of that cost */
      simgrid::s4u::this_actor::execute(compute_cost);
  } while (compute_cost > 0); /* Stop when receiving an invalid compute_cost */

  XBT_INFO("Exiting now.");
}


/*
Most actors are started from the deployment XML file because this is a better scientific habit,
From XML with simgrid::s4u::Engine::register_actor() (if your actor is a class)
or simgrid::s4u::Engine::register_function() (if your actor is a function)
and then simgrid::s4u::Engine::load_deployment()

*/

int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);

  /* Register the functions representing the actors */
  e.register_function("master", &master);
  e.register_function("worker", &worker);

  /* Load the platform description and then deploy the application */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);

  /* Run the simulation */
  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}
