#include <iostream>
#include "simgrid/s4u.hpp"
#include <cstdlib>
#include <vector>
#include <string>
#include <sstream>



XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");

long nb_hosts;
std::vector<simgrid::s4u::Host*> hosts; //needd to be global to have it in the master 
                                        //because the host name need the engin  

//**********************************************************
//-----------------------Functions--------------------------
//**********************************************************

//----------find mailboxes to read from -----------------//
inline const char* find_rec_mailbox(std::vector<std::string> args)
{
  const char* key= "rec_mail";
  return (simgrid::s4u::Actor::self()->get_property(key)) ; 
}

//----------find number of dependency to read from-------//
inline int find_num_dep(std::vector<std::string> args)
{ 
  int depnd_nm; 
  const char* dep_key= "dep";    
  const char* numdep= simgrid::s4u::Actor::self()->get_property(dep_key); 
    
  if( numdep != nullptr ){
    depnd_nm=  std::stol(std::string(numdep)); // if the number is mentioned then store it 
  } 
  else depnd_nm=1; //else there is just one dependency and one message need to be read 

  return depnd_nm;
}

//----------get number of mailboxes to send to (depend) -------//
inline std::vector<std::string> get_sendingTo_mails(std::vector<std::string> args)
{
  std::vector<std::string> send_mail_names;
  const char* key_s;
  const char* namebox_s;
  int keyid_s=1;
  do {

    key_s=("send_mail" + std::to_string(keyid_s)).c_str();  // https://stackoverflow.com/questions/22440067/concatenating-integers-to-const-char-strings
    //XBT_INFO("key mail is: %s", ("mail" + std::to_string(keyid)).c_str());
    namebox_s= simgrid::s4u::Actor::self()->get_property(key_s); 
    
    if (namebox_s != nullptr ){ 
      send_mail_names.push_back(std::string(namebox_s)) ;
      //XBT_INFO("app_drop sender mail number %d is: %s",keyid_s, name_box_s.c_str()  );
      keyid_s++; 
    }

  }while(namebox_s != nullptr ); 
  
  return send_mail_names;
}






static void data_drop(std::vector<std::string> args)
{  
  double compute_cost ;
  long communication_cost ;

  if(args.size()>1){ // master is app_drop contains so many data control 
    compute_cost             = std::stod(args[1]); 
    communication_cost       = std::stol(args[2]);
  }

  //---------------------------------------------------------------
  // ---------------------recive messages -------------------------
  //---------------------------------------------------------------

  const char* namebox= find_rec_mailbox(args); // cal the function to know the mailboxes need to be read 
  
  if ( namebox != nullptr) // there is reading part 
  { 
    simgrid::s4u::Mailbox* re_mailbox   = simgrid::s4u::Mailbox::by_name(namebox); // notice here set the name of the mail box associated to the name of the host which in master ot fowarder 
    
    int depnd_nm= find_num_dep( args);
    //XBT_INFO("data_drop recive mail is: %s and read as dep num %d ", namebox , depnd_nm );
    
    std::vector<const char* > messgae_rec; 
    
    for ( int i=0 ; i<depnd_nm ; i++){
      const char* *msg;
      simgrid::s4u::CommPtr comm = re_mailbox->get_async<const char*>(&msg);
      comm->wait();
      messgae_rec.push_back(*msg);
      //XBT_INFO("data_drop mail is: %s and I recived %s", namebox, *msg );
    }
    
    if ( messgae_rec.size()== depnd_nm){ 
      int yes;
      for(int i=0; i<messgae_rec.size() ;i++ ){
        if (strcmp(messgae_rec[i], "C-D") == 0) yes++;
      }
      if(yes==depnd_nm ) simgrid::s4u::this_actor::execute(compute_cost);
    }
  }

  //the sending message represent the end of the data drop
  //-----------------------------------------------------------
  // ---------------------send messages------------------------
  //-----------------------------------------------------------

  std::vector<std::string> send_mails= get_sendingTo_mails(args);
  
  for(int i=0; i<send_mails.size();i++){
    simgrid::s4u::Mailbox* se_mailbox = simgrid::s4u::Mailbox::by_name(send_mails[i]);
    //se_mailbox->put_async(new double(100), 1.0);
    simgrid::s4u::CommPtr comm = se_mailbox->put_async(new const char*("C-D") , 10);// send D as complete of data drop 
    comm->wait();
  }
  XBT_INFO("Exiting now data_drop.");

}



static void app_drop(std::vector<std::string> args)
{ 
  double compute_cost ;
  long communication_cost ;

  if(args.size()>1){ // master is app_drop contains so many data control 
    compute_cost             = std::stod(args[1]); 
    communication_cost       = std::stol(args[2]);
  }
  
  // app_dropcan send to multiple data drops too that should be considered
  //xbt_assert(args.size() == 1, "The app_dropexpects no argument");

  // create resulted data drop and here you should not mention the same forwarder in xml 
  // const char* keys= "create";
  // const char* keydepp= simgrid::s4u::Actor::self()->get_property(keys); 
  // if(keydepp != nullptr ) //not all the actors will have this
  //  {  
  //  //XBT_INFO(" name of host %s", simgrid::s4u::this_actor::get_name());
  //  if(std::stol(std::string(keydepp))  > 0){
  //   XBT_INFO(" app_dropforward dependency %d",std::stol(std::string(keydepp))  );

  //    std::vector<simgrid::s4u::ActorPtr> actor;

  //    for ( int i=0; i<std::stol(std::string(keydepp)) ;i++){
  //      std::vector<std::string> args; 
  //      actor.push_back( simgrid::s4u::Actor::create("forwarder-mem"+std::to_string(i)
  //                 ,simgrid::s4u::Host::by_name(hosts[i]->get_cname()), forwarder,args) );   
  //    }
  //   }
  //  }
  

  // reading from multiple data drops based on the number of dependancy 
  // and in ths way all the prescussor send to on mail and read it many times based on the number of dep 
  //---------------------------------------------------------------
  // ---------------------recive messages -------------------------
  //---------------------------------------------------------------
  int depnd_nm= find_num_dep( args);
  
  if( depnd_nm == 0 ) { //master case sould execute without any expected data  
    simgrid::s4u::this_actor::execute(compute_cost);
  }

  const char* namebox= find_rec_mailbox(args);
  if ( namebox != nullptr) // there is reading pat 
  { 
    simgrid::s4u::Mailbox* re_mailbox   = simgrid::s4u::Mailbox::by_name(namebox); // notice here set the name of the mail box associated to the name of the host which in master ot fowarder 
    //XBT_INFO("app_droprecive mail is: %s and read as dep num %d ", namebox , depnd_nm );
    std::vector<const char* > messgae_rec; 
    for ( int i=0 ; i<depnd_nm ; i++){
      const char* *msg;
      simgrid::s4u::CommPtr comm = re_mailbox->get_async<const char*>(&msg);
      comm->wait();
      messgae_rec.push_back(*msg);
       //XBT_INFO("app_drop mail is: %s and I recived %s", namebox, *msg );
    }
   
   // app_drop recived end of data drop so it can compute his data
   if ( messgae_rec.size()== depnd_nm){ 
      int yes;
      for(int i=0; i<messgae_rec.size() ;i++ ){
        if (strcmp(messgae_rec[i], "C-D") == 0) yes++;
      }
      if(yes==depnd_nm )simgrid::s4u::this_actor::execute(compute_cost);
    }
  } 

  //-----------------------------------------------------------
  // ---------------------send messages------------------------
  //-----------------------------------------------------------
  // multi sending from the app to data drops  
  std::vector<std::string> send_mails= get_sendingTo_mails(args);
  // when the app drop finish execution then send ACK to Data Drop to start 
  for(int i=0; i<send_mails.size();i++){
    simgrid::s4u::Mailbox* se_mailbox = simgrid::s4u::Mailbox::by_name(send_mails[i]);
    //se_mailbox->put_async(new double(100), 1.0);
    simgrid::s4u::CommPtr comm = se_mailbox->put_async(new const char*("C-A")  , 10);  
    comm->wait();
  }
       
  XBT_INFO("Exiting now app_drop .");
}






 // no need to send -1 to stop becauese while is not based on this 
 // but to keep the actor working and waiting for new data from the precessor 
 // you can make ack from each data_drop and suspend the actor until new data 
 //arrive or keep it waiting. but with time threshold 
 // comm = se_mailbox->put_async(new double(-1) , 100);  
 // comm->wait(); 


int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);

  /* Register the functions representing the actors */
  e.register_function("data_drop", &data_drop);
  e.register_function("app_drop", &app_drop );

  /* Load the platform description and then deploy the application */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
  
  hosts=e.get_all_hosts(); //global 
  nb_hosts= e.get_host_count(); // global var number of hosts which will be number of actors 
  
  /* Run the simulation */
  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}
