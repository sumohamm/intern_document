#include <iostream>
#include "simgrid/s4u.hpp"
#include <cstdlib>
#include <vector>
#include <string>
#include <sstream>

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");
double compute_cost ;
long communication_cost ;
long nb_hosts;
std::vector<simgrid::s4u::Host*> hosts; //needd to be global to have it in the master 
                                        //because the host name need the engin  

static void master(std::vector<std::string> args)
{
  
  // XBT_INFO( " ardgs %c" , args[0] ) ;
  // //xbt_assert(args.size() > 4, "The master function expects at least 3 arguments");

  // long forwarders_num       = std::stol(args[1]); // number of forwareder 
  // double compute_cost       = std::stod(args[2]); 
  // long communication_cost   = std::stol(args[3]);
  // long num_tasks_on_workers = std::stol(args[4]);
  // std::vector<simgrid::s4u::Mailbox*> forwarders; //you need multible mailboxes , each mail has unique name like twitter hastage 
  
  // // notice here i= 4 to args.size() and then by_name(args[i])), because the firt three args are count ,comp_count,commcost, 
  // // and then after arg[4] until the end are the name of hosts of forowrders , see deploy file 

  // for (unsigned int i = 5; i < args.size(); i++)
  //   forwarders.push_back(simgrid::s4u::Mailbox::by_name(args[i])); // the mailbox named by the name of the hosts the forwarders will work on 

  // //XBT_INFO("Got %zu forwarders and %ld tasks to process", forwarders.size(), tasks_count);

  // for (int i = 0; i < forwarders_num; i++) { /* For each task to be executed: */
  //   /* - Select a c in a round-robin way */
  //   simgrid::s4u::Mailbox* mailbox = forwarders[i % forwarders.size()];

  //   /* - Send the computation cost to that forwarders */
  //   XBT_INFO("Sending to forwarder %d of %ld to mailbox '%s'", i+1, forwarders_num, mailbox->get_cname());
    
  //   mailbox->put(new double(10) , 10000); 
  // }

  // here no need to send -1 for forward because it will not perform  
}



/*
as you can see that mem3 forwarder will be tried to executed since the start 
the best is that the actor generate the forwarder 
*/
static void forwarder(std::vector<std::string> args)
{   
  // recive messages 
  //xbt_assert(argc >= 3, "Actor forwarder requires 2 parameters, but got only %d", argc - 1);
   const char* key_rec= "rec_mail";
   const char* box_rec= simgrid::s4u::Actor::self()->get_property(key_rec); 
    
   XBT_INFO("forwarder recive mail is: %s", box_rec );
   // add if to check the existance of rec 
   simgrid::s4u::Mailbox* re_mailbox   = simgrid::s4u::Mailbox::by_name(box_rec); // notice here set the name of the mail box associated to the name of the host which in master ot fowarder 


   auto msg                    = re_mailbox->get_unique<double>();// get unique no need to free mem 
   double compute_cost_rec= *msg; 
   simgrid::s4u::this_actor::execute(compute_cost_rec); // time of storing data 
   
   // ---------------------send messages-------------------------
    //-----------------------------------------------------------

   const char* key;
   const char* namebox;
   int keyid=1; 
   do{
    key=("send_mail" + std::to_string(keyid)).c_str();  // https://stackoverflow.com/questions/22440067/concatenating-integers-to-const-char-strings
    //XBT_INFO("key mail is: %s", ("mail" + std::to_string(keyid)).c_str());

    namebox= simgrid::s4u::Actor::self()->get_property(key); 
    if (namebox != nullptr ){ 
      std::string name_box= namebox;
      XBT_INFO("forwarder sender mail number %d is: %s",keyid, name_box.c_str()  );
      
      simgrid::s4u::Mailbox* se_mailbox = simgrid::s4u::Mailbox::by_name(name_box);
      //se_mailbox->put_async(new double(100), 1.0);
      simgrid::s4u::CommPtr comm = se_mailbox->put_async(new double(10.0) , 100);  
      comm->wait();

      // comm = se_mailbox->put_async(new double(-1) , 100);  
      // comm->wait();
      keyid++; 
    }
  }while(namebox != nullptr ); 

  // for (unsigned int i = 0; i < workers.size(); i++) {
  //   simgrid::s4u::Mailbox* mailbox = workers[i % workers.size()];
  //   mailbox->put(new double(-1.0), 0);
  //}
  XBT_INFO("Exiting now forwarder.");
}



static void worker(std::vector<std::string> args)
{ 
  if(args.size()>2) // master is worker contains so many data control 
  {
     compute_cost             = std::stod(args[1]); 
     communication_cost       = std::stol(args[2]);
  }
  
  // worker can send to multiple data drops too that should be considered
  //xbt_assert(args.size() == 1, "The worker expects no argument");

  // create resulted data drop and here you should not mention the same forwarder in xml 
  // const char* keys= "create";
  // const char* keydepp= simgrid::s4u::Actor::self()->get_property(keys); 
  // if(keydepp != nullptr ) //not all the actors will have this
  //  {  
  //  //XBT_INFO(" name of host %s", simgrid::s4u::this_actor::get_name());
  //  if(std::stol(std::string(keydepp))  > 0){
  //   XBT_INFO(" worker forward dependency %d",std::stol(std::string(keydepp))  );

  //    std::vector<simgrid::s4u::ActorPtr> actor;

  //    for ( int i=0; i<std::stol(std::string(keydepp)) ;i++){
  //      std::vector<std::string> args; 
  //      actor.push_back( simgrid::s4u::Actor::create("forwarder-mem"+std::to_string(i)
  //                 ,simgrid::s4u::Host::by_name(hosts[i]->get_cname()), forwarder,args) );   
  //    }
  //   }
  //  }
  
  // reading from multiple data drops 
  

  int depnd_nm;
  const char* key= "rec_mail";
  const char* namebox= simgrid::s4u::Actor::self()->get_property(key); 
  if ( namebox != nullptr) // there is reading pat 
  { 
    simgrid::s4u::Mailbox* re_mailbox   = simgrid::s4u::Mailbox::by_name(namebox); // notice here set the name of the mail box associated to the name of the host which in master ot fowarder 

    const char* dep_key= "dep";
    const char* numdep= simgrid::s4u::Actor::self()->get_property(dep_key); 
    
    XBT_INFO("worker recive mail is: %s and read as dep num %d ", namebox , numdep );

    if( numdep != nullptr ){
      depnd_nm=  std::stol(std::string(numdep)); // if the number is mentioned then store it 
    } 
    else depnd_nm=1; //else there is just one dependency and one message need to be read 
    XBT_INFO("depnd_nm %d",depnd_nm);
      for ( int i=0 ; i<depnd_nm ; i++){

        double compute_cost;
        double *msg;

        simgrid::s4u::CommPtr comm = re_mailbox->get_async<double>(&msg);
        comm->wait();
        compute_cost = *msg;
        
        XBT_INFO("worker mail is: %s and I recived %f", namebox, compute_cost );
        simgrid::s4u::this_actor::execute(compute_cost);
        /* If compute_cost is valid, execute a computation of that cost */
    }
  } 

  // sending from the app to data drop 
   const char* key_s;
   const char* namebox_s;
   int keyid_s=1; 
  
   do{
    key_s=("send_mail" + std::to_string(keyid_s)).c_str();  // https://stackoverflow.com/questions/22440067/concatenating-integers-to-const-char-strings
    //XBT_INFO("key mail is: %s", ("mail" + std::to_string(keyid)).c_str());

    namebox_s= simgrid::s4u::Actor::self()->get_property(key_s); 
    if (namebox_s != nullptr ){ 
      std::string name_box_s= namebox_s;
      XBT_INFO("worker sender mail number %d is: %s",keyid_s, name_box_s.c_str()  );
      
      simgrid::s4u::Mailbox* se_mailbox = simgrid::s4u::Mailbox::by_name(name_box_s);
      //se_mailbox->put_async(new double(100), 1.0);
      simgrid::s4u::CommPtr comm = se_mailbox->put_async(new double(10.0) , 100);  
      comm->wait();
      
      // no need to send -1 to stop becauese while is not based on this 
      // but to keep the actor working and waiting for new data from the precessor 
      // you can make ack from each forwarder and suspend the actor until new data 
      //arrive or keep it waiting. but with time threshold 
      // comm = se_mailbox->put_async(new double(-1) , 100);  
      // comm->wait();
      keyid_s++; 
    }
    

  }while(namebox_s != nullptr ); 
  //simgrid::s4u::Actor::self()->kill();
  XBT_INFO("Exiting now worker.");
}




int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);

  /* Register the functions representing the actors */
  //e.register_function("master", &master);
  e.register_function("forwarder", &forwarder);
  e.register_function("worker", &worker);

  /* Load the platform description and then deploy the application */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
  
  hosts=e.get_all_hosts(); //global 
  nb_hosts= e.get_host_count(); // global var number of hosts which will be number of actors 
  
  /* Run the simulation */
  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}
