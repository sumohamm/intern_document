#include <iostream>
#include "simgrid/s4u.hpp"
#include <cstdlib>
#include <vector>
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");

static void master(std::vector<std::string> args)
{
  xbt_assert(args.size() > 4, "The master function expects at least 3 arguments");

  long forwarders_num       = std::stol(args[1]); // number of forwareder 
  double compute_cost       = std::stod(args[2]); 
  long communication_cost   = std::stol(args[3]);
  long num_tasks_on_workers = std::stol(args[4]);
  std::vector<simgrid::s4u::Mailbox*> forwarders; //you need multible mailboxes , each mail has unique name like twitter hastage 
  
  // notice here i= 4 to args.size() and then by_name(args[i])), because the firt three args are count ,comp_count,commcost, 
  // and then after arg[4] until the end are the name of hosts of forowrders , see deploy file 

  for (unsigned int i = 5; i < args.size(); i++)
    forwarders.push_back(simgrid::s4u::Mailbox::by_name(args[i])); // the mailbox named by the name of the hosts the forwarders will work on 

  //XBT_INFO("Got %zu forwarders and %ld tasks to process", forwarders.size(), tasks_count);

  for (int i = 0; i < forwarders_num; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */
    simgrid::s4u::Mailbox* mailbox = forwarders[i % forwarders.size()];

    /* - Send the computation cost to that forwarders */
    XBT_INFO("Sending to forwarder %d of %ld to mailbox '%s'", i+1, forwarders_num, mailbox->get_cname());
    mailbox->put(new double(compute_cost) , 0.0);
    //mailbox->put(new double(1000), 0.0);
    mailbox->put(new double(num_tasks_on_workers),0.0);
  }

  // here no need to send -1 for forward because it will not perform  
}



static void worker(std::vector<std::string> args)
{
  xbt_assert(args.size() == 1, "The worker expects no argument");

  const simgrid::s4u::Host* my_host = simgrid::s4u::this_actor::get_host();
  simgrid::s4u::Mailbox* mailbox   = simgrid::s4u::Mailbox::by_name(my_host->get_name()); // notice here set the name of the mail box associated to the name of the host which in master ot fowarder 

  double compute_cost;
  do {
    auto msg     = mailbox->get_unique<double>();
    compute_cost = *msg;
    XBT_INFO("worker msg: %f", compute_cost);
    if (compute_cost > 0) /* If compute_cost is valid, execute a computation of that cost */
      simgrid::s4u::this_actor::execute(compute_cost);
  } while (compute_cost > 0); /* Stop when receiving an invalid compute_cost */

  XBT_INFO("Exiting now.");
}


static void forwarder(std::vector<std::string> args)
{  
   // recive messages 
  //xbt_assert(argc >= 3, "Actor forwarder requires 2 parameters, but got only %d", argc - 1);
   const simgrid::s4u::Host* my_host = simgrid::s4u::this_actor::get_host();
   simgrid::s4u::Mailbox* mailbox   = simgrid::s4u::Mailbox::by_name(my_host->get_name()); // notice here set the name of the mail box associated to the name of the host which in master ot fowarder 
   

   auto msg                    = mailbox->get_unique<double>();
   double compute_cost= *msg; 

   XBT_INFO("comput_cost  %f",compute_cost); // 0 not empty , 1  yes 
  
   auto msg2                    = mailbox->get<double>();
   double num_tasks=*msg2;
   XBT_INFO("num_tasks =%f", num_tasks);

  

  //creat workers 
  std::vector<simgrid::s4u::Mailbox*> workers;
  for (unsigned int i = 1; i < args.size(); i++)
      workers.push_back(simgrid::s4u::Mailbox::by_name(args[i])); 
 
  for (int i = 0; i < num_tasks; i++) { /* For each task to be executed: */
    /* - Select a c in a round-robin way */
    simgrid::s4u::Mailbox* mailbox = workers[i % workers.size()];
    mailbox->put(new double(compute_cost), 10000);
  }

  for (unsigned int i = 0; i < workers.size(); i++) {
    simgrid::s4u::Mailbox* mailbox = workers[i % workers.size()];
    mailbox->put(new double(-1.0), 0);
  }
}


/*
Most actors are started from the deployment XML file because this is a better scientific habit, 
From XML with simgrid::s4u::Engine::register_actor() (if your actor is a class) 
or simgrid::s4u::Engine::register_function() (if your actor is a function)
and then simgrid::s4u::Engine::load_deployment()

*/

int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);

  /* Register the functions representing the actors */
  e.register_function("master", &master);
  e.register_function("forwarder", &forwarder);
  e.register_function("worker", &worker);

  /* Load the platform description and then deploy the application */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);

  /* Run the simulation */
  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}
